/*
 * Copyright 2015 Bhavit Singh Sengar
 * Copyright 2015-2016 Hans-Christoph Steiner
 * Copyright 2015-2016 Nathan Freitas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * From https://stackoverflow.com/a/29946540
 */

package com.example.justnetcipher.client;

import android.net.SSLCertificateSocketFactory;
import android.os.Build;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

// alternate version that includes a socket with a constructor that disables SNI and ignores hostnames
public class TlsOnlySocketFactoryNoSni extends TlsOnlySocketFactory {

    public TlsOnlySocketFactoryNoSni() {
        super();
    }

    public TlsOnlySocketFactoryNoSni(SSLSocketFactory delegate) {
        super(delegate);
    }

    public TlsOnlySocketFactoryNoSni(SSLSocketFactory delegate, boolean compatible) {
        super(delegate, compatible);
    }

    // alternate version that creates a socket that disables SNI
    private Socket makeSocketSafe(Socket socket, String host) {
        if (socket instanceof SSLSocket) {
            TlsOnlySSLSocketNoSni tempSocket =
                    new TlsOnlySSLSocketNoSni((SSLSocket) socket, compatible);

            // with sni disabled, there is no reason to set hostname
            if (delegate instanceof SSLCertificateSocketFactory &&
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                SSLCertificateSocketFactory factory = (SSLCertificateSocketFactory) delegate;
                factory.setUseSessionTickets(socket, false);
            }

            socket = tempSocket;
        }
        return socket;
    }

    // override these methods to ensure local version of makeSocketSafe() is called
    @Override
    public Socket createSocket(Socket s, String host, int port, boolean autoClose)
            throws IOException {
        return makeSocketSafe(delegate.createSocket(s, host, port, autoClose), host);
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException {
        return makeSocketSafe(delegate.createSocket(host, port), host);
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localHost, int localPort)
            throws IOException {
        return makeSocketSafe(delegate.createSocket(host, port, localHost, localPort), host);
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        return makeSocketSafe(delegate.createSocket(host, port), host.getHostName());
    }

    @Override
    public Socket createSocket(InetAddress address, int port, InetAddress localAddress,
                               int localPort) throws IOException {
        return makeSocketSafe(delegate.createSocket(address, port, localAddress, localPort),
                address.getHostName());
    }

    private class TlsOnlySSLSocketNoSni extends DelegateSSLSocket {

        final boolean compatible;

        private TlsOnlySSLSocketNoSni(SSLSocket delegate, boolean compatible) {
            super(delegate);

            // ensure SNI is disabled
            toggleSni(false);

            this.compatible = compatible;

            // badly configured servers can't handle a good config
            if (compatible) {
                ArrayList<String> protocols = new ArrayList<String>(Arrays.asList(delegate
                        .getEnabledProtocols()));
                protocols.remove(SSLV2);
                protocols.remove(SSLV3);
                super.setEnabledProtocols(protocols.toArray(new String[protocols.size()]));

                /*
                 * Exclude extremely weak EXPORT ciphers. NULL ciphers should
                 * never even have been an option in TLS.
                 */
                ArrayList<String> enabled = new ArrayList<String>(10);
                Pattern exclude = Pattern.compile(".*(EXPORT|NULL).*");
                for (String cipher : delegate.getEnabledCipherSuites()) {
                    if (!exclude.matcher(cipher).matches()) {
                        enabled.add(cipher);
                    }
                }
                super.setEnabledCipherSuites(enabled.toArray(new String[enabled.size()]));
                return;
            } // else

            // 16-19 support v1.1 and v1.2 but only by default starting in 20+
            // https://developer.android.com/reference/javax/net/ssl/SSLSocket.html
            ArrayList<String> protocols = new ArrayList<String>(Arrays.asList(delegate
                    .getSupportedProtocols()));
            protocols.remove(SSLV2);
            protocols.remove(SSLV3);
            if (Build.VERSION.SDK_INT >= 24) {
                protocols.remove(TLSV1);
                protocols.remove(TLSV1_1);
            }
            super.setEnabledProtocols(protocols.toArray(new String[protocols.size()]));

            /*
             * Exclude weak ciphers, like EXPORT, MD5, DES, and DH. NULL ciphers
             * should never even have been an option in TLS.
             */
            ArrayList<String> enabledCiphers = new ArrayList<String>(10);
            Pattern exclude = Pattern.compile(".*(_DES|DH_|DSS|EXPORT|MD5|NULL|RC4|TLS_FALLBACK_SCSV).*");
            for (String cipher : delegate.getSupportedCipherSuites()) {
                if (!exclude.matcher(cipher).matches()) {
                    enabledCiphers.add(cipher);
                }
            }
            super.setEnabledCipherSuites(enabledCiphers.toArray(new String[enabledCiphers.size()]));
        }

        @Override
        public DelegateSSLSocket setHostname(String host) {

            try {
                // SSLSocket no longer includes setHostname method so this method must be used
                // setting the parameter to null is consistent with disabling SNI
                SSLParameters sslParameters = delegate.getSSLParameters();
                sslParameters.setServerNames(null);
                delegate.setSSLParameters(sslParameters);
            } catch (Exception e) {
                throw new IllegalStateException("Could not disable SNI", e);
            }

            return (this);
        }
    }
}
