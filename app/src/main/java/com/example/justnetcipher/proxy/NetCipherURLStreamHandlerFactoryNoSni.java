/*
 * Copyright 2019 Hans-Christoph Steiner
 * Copyright 2019 Michael Pöhn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.justnetcipher.proxy;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.annotation.Nullable;

import java.net.URLStreamHandler;

public class NetCipherURLStreamHandlerFactoryNoSni extends NetCipherURLStreamHandlerFactory {

    public static final String TAG = "NetCipherURL_Factory_NS";

    // alternate version that creates a stream handler that opens a connection with a socket factory that disables SNI
    @SuppressLint("PrivateApi")
    @Nullable
    @Override
    public URLStreamHandler createURLStreamHandler(String protocol) {
        try {
            switch (protocol) {
                case "http":
                    return new NetCipherURLStreamHandlerNoSni(
                            (URLStreamHandler) Class.forName("com.android.okhttp.HttpHandler").newInstance());
                case "https":
                    return new NetCipherURLStreamHandlerNoSni(
                            (URLStreamHandler) Class.forName("com.android.okhttp.HttpsHandler").newInstance());
                case "file":
                    // this does not use a netcipher class so SNI may be enabled
                    return (URLStreamHandler) Class.forName("sun.net.www.protocol.file.Handler").newInstance();
                default:
                    Log.e(TAG, "Unsupported protocol: " + protocol);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
